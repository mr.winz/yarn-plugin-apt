import { Plugin } from '@yarnpkg/core';
import AptAddCommand from './command/AptAddCommand';
import AptInstallCommand from './command/AptInstallCommand';
import AptResetCommand from './command/AptResetCommand';
import AptFetcher from './fetcher/AptFetcher';
import AptRelaxedResolver from './resolver/AptRelaxedResolver';
import AptStrictResolver from './resolver/AptStrictResolver';
import { afterAllInstalled } from './common/hooks';

const plugin: Plugin = {
  commands: [
    AptResetCommand,
    AptAddCommand,
    AptInstallCommand
  ],
  fetchers: [
    AptFetcher,
  ],
  resolvers: [
    AptRelaxedResolver,
    AptStrictResolver
  ],
  hooks: {afterAllInstalled}
};

// eslint-disable-next-line arca/no-default-export
export default plugin;